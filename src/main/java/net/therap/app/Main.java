package net.therap.app;

import net.therap.domain.Menu;

import java.sql.SQLException;
import java.util.Scanner;

/**
 * @author altamas.haque
 * @since 2/9/20
 */
public class Main {
    public static void main(String args[]) {
        try {
            String url = "jdbc:mysql://localhost:3306/therap";
            String user = "root";
            String password = "password";

            Menu menu = new Menu(url, user, password);
            menu.init();


            System.out.println("Welcome to Therap BD Ltd.");
            while (true) {
                System.out.println("Choose one of the following options...");
                System.out.println("0: Exit");
                System.out.println("1: Show menu");
                System.out.println("2: Change Breakfast Item");
                System.out.println("3: Change Lunch Item");

                Scanner scanner = new Scanner(System.in);
                System.out.print("Enter your option: ");
                int option = scanner.nextInt();
                if (option == 0) {
                    menu.close();
                    break;
                }

                if (option == 1) {
                    menu.printMenu();
                }

                if (option == 2) {
                    System.out.println("Changing Breakfast Item...");
                    System.out.print("Enter Id: ");
                    int id = scanner.nextInt();
                    scanner.nextLine();

                    System.out.print("Enter Item: ");
                    String item = scanner.nextLine();

                    menu.changeBreakfastItem(id, item);
                    System.out.println("Breakfast item changed.");
                    menu.printMenu();
                }

                if (option == 3) {
                    System.out.println("Changing Lunch Item...");
                    System.out.print("Enter Id: ");
                    int id = scanner.nextInt();
                    scanner.nextLine();

                    System.out.print("Enter Item: ");
                    String item = scanner.nextLine();

                    menu.changeLunchItem(id, item);
                    System.out.println("Lunch item changed.");
                    menu.printMenu();
                }
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
