package net.therap.domain;

import java.sql.*;

/**
 * @author altamas.haque
 * @since 2/10/20
 */
public class Menu {
    private String url;
    private String user;
    private String password;
    private String tableName;
    private Connection connection;

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public Menu(String url, String user, String password) {
        this.url = url;
        this.user = user;
        this.password = password;
        this.tableName = "menu";
    }

    public void init() throws ClassNotFoundException, SQLException {
        this.connection = DriverManager.getConnection(url, user, password);
    }

    public void printMenu() throws SQLException {
        String query = "SELECT * FROM " + this.tableName + ";";
        PreparedStatement statement = this.connection.prepareStatement(query);
        ResultSet resultSet = statement.executeQuery();

        System.out.println(String.format("%62s", "").replace(" ", "-"));
        System.out.println(String.format("| %-5s | %-15s | %-15s | %-15s |", "Id", "Day", "Breakfast", "Lunch"));
        System.out.println(String.format("%62s", "").replace(" ", "-"));

        while (resultSet.next()) {
            int id = resultSet.getInt(1);
            String day = resultSet.getString(2);
            String breakfast = resultSet.getString(3);
            String lunch = resultSet.getString(4);
            System.out.println(String.format("| %5s | %15s | %15s | %15s |", Integer.toString(id), day, breakfast, lunch));
            System.out.println(String.format("%62s", "").replace(" ", "-"));
        }
    }

    public void changeLunchItem(int id, String lunch) throws SQLException {
        String editLunchQuery = "UPDATE " + this.tableName + " SET lunch=? WHERE id=?;";
        PreparedStatement editLunch = this.connection.prepareStatement(editLunchQuery);
        editLunch.setString(1, lunch);
        editLunch.setInt(2, id);
        editLunch.executeUpdate();
    }

    public void changeBreakfastItem(int id, String breakfast) throws SQLException {
        String editBreakfastQuery = "UPDATE " + this.tableName + " SET breakfast=? WHERE id=?;";
        PreparedStatement editBreakfast = this.connection.prepareStatement(editBreakfastQuery);
        editBreakfast.setString(1, breakfast);
        editBreakfast.setInt(2, id);
        editBreakfast.executeUpdate();
    }

    public void close() throws SQLException {
        this.connection.close();
    }
}
